Workspace congiguration file
===============================

tl;dr
-----

Unpack/Clone workspace tarball/repo and run bootstrap
Clear dependencies as you go

About Security
--------------

SSH private key, AWS API secret and what not shall be stored elsewhere and
mount to their appropriate volume where they make sense
